FROM debian:buster-slim

RUN apt update -y && apt upgrade -y
RUN apt install -y openssh-server rsyslog git ca-certificates

COPY entrypoint.sh entrypoint.sh
COPY bin/shutdown /sbin/shutdown
COPY bin/shutdown /sbin/reboot
RUN chmod +x entrypoint.sh /sbin/reboot /sbin/shutdown

VOLUME ["/home"]

EXPOSE 22

CMD ["/entrypoint.sh"]
