#!/bin/bash

touch /var/log/auth.log
touch /var/log/syslog
touch /home/packages
touch /home/services


# Disable load module
sed -i 's/module(load="imklog") /#module(load="imklog")/' /etc/rsyslog.conf 
# Permit Root SSH Login
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

## Custom install
while IFS= read -r line
do
  echo "$line"
	apt install -y $line
done < <(cat /home/packages)

# Root Passw
if [ ! -f /home/.root_passw ]; then
	echo ---
	echo
	PASS=`openssl rand -base64 24`
	echo root:$PASS | chpasswd
	echo $PASS
	touch /home/.root_passw
	echo
	echo ---
	echo
else
	echo ---
	echo PASSWORD YET CREATED
	echo ---
fi

service rsyslog ssh start

## Custom services
while IFS= read -r line
do
  echo "$line"
	service $line start
done < <(cat /home/services)

tail -f /var/log/auth.log
