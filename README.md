# GNU/Linux Debian image for development

## Configure

Configure custom variables on _.env_

Start container

```
docker-compose up -d
```

Read logs for root password

```
docker-compose logs -f
```

**Only /home directory is persistent**

## User configure

Configuration files

* /home/packages : Install packages on restart container
* /home/services : Start services on restart container

Add packages on _/home/packages_ (like _apt install <package>_) and _reboot_
Add services on _/home/services)_ (like _service <service> start_) and _reboot_

Example:

/home/packages

```
nginx-full
php-fpm
```

/home/services

```
nginx
```
